package Drones;

import java.util.Scanner;

 class DroneInterface {

	 private final DroneArena droneArena;// arena in which drones are shown
    /**
    	 * constructor for DroneInterface
    	 * sets up scanner used for input and the arena
    	 * then has main loop allowing user to enter commands
     */
    public DroneInterface() {
		// scanner used for input from user
		Scanner scanner = new Scanner(System.in);// set up scanner for user input
		droneArena = new DroneArena(20, 20);// create arena of size 20*6
        char ch;
        do {
        	System.out.print("Enter A: add drone, I: get Information, X: exit,  D: Display, M: move.");
        	ch = scanner.next().charAt(0);
        	scanner.nextLine();
        	switch (ch) {
				case 'A' :
				case 'a' :
					droneArena.addDrone();	// add a new drone to arena
					break;
        		case 'I' :
        		case 'i' :
        			System.out.print(droneArena.toString());
					break;
        		case 'x' :
        			ch = 'X';				// when X detected program ends
					break;
        		case 'd' :
				case 'D' :
					doDisplay();
				case 'm' :
				case 'M' :			
					droneArena.moveAllDrones(droneArena);
					doDisplay();
        	}
        } while (ch != 'X');
       scanner.close();
    }
	void doDisplay() {
	 int x = droneArena.getX(droneArena);
	 int y = droneArena.getY(droneArena);
	 ConsoleCanvas Arena = new ConsoleCanvas(x,y);
	 droneArena.showDrones(Arena);
	 System.out.println(Arena.toString());
 	}

	public static void main(String[] args) {
		new DroneInterface();
	}

}