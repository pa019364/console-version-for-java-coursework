package Drones;

import Drones.Direction.NESW;

public class Drone {
    private static int droneCount;
    private int xPos;
	private int yPos;
	private final int droneId;
	private NESW test;

    Drone(int x, int y, NESW w) {
        xPos = x;
        yPos = y;
        droneId = droneCount++;
        test = w;
    }

    public static void main(String[] args) {
        Drone d = new Drone(5, 3, NESW.South); // create drone
        System.out.println(d.toString()); // print where is
        Drone e = new Drone(6, 4, NESW.North);
        System.out.println(e.toString());
    }

    public String toString() {
        return "Drone " + droneId + " is at " + xPos + "," + yPos + " and is travelling in the " + test.name()
                + " direction.";
    }

    public boolean isHere(int sx, int sy) {
		return (xPos == sx) && (yPos == sy);
    }

    public void displayDrone(ConsoleCanvas c) {
        c.showIt(xPos, yPos, "D");
    }

    public void tryToMove(DroneArena arena) {
		int directionX;
		int directionY;
		switch (test.name()) {
			case "North":
				directionX = -1;
				if ((arena.canMoveHere(xPos + directionX, yPos))) {
					xPos = xPos + directionX;

				} else {
					test = test.getNextDirection();
				}
				break;
			case "East":

				directionY = 1;
				if ((arena.canMoveHere(xPos, yPos + directionY))) {
					yPos = yPos + directionY;
				} else {
					test = test.getNextDirection();
				}
				break;
			case "South":
				directionX = 1;
				if ((arena.canMoveHere(xPos + directionX, yPos))) {
					xPos = xPos + directionX;
				} else {
					test = test.getNextDirection();
				}
				break;
			case "West":
				directionY = -1;
				if ((arena.canMoveHere(xPos, yPos + directionY))) {
					yPos = yPos + directionY;
				} else {
					test = test.getNextDirection();
				}
				break;
		}
    }
}
